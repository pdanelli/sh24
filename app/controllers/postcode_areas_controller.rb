# frozen_string_literal: true

class PostcodeAreasController < ApplicationController
  def create
    # Update our postcode area with the data from the form
    postcode_area << permitted_params

    # Perform basic validation on the postcode to ensure its ready to be sent to the API
    @result = if postcode_area.valid?
                t("postcode_area.service_area.result.#{service_area_status}")
              else
                t("postcode_area.postcode.invalid")
              end

    render :new
  end

  def postcode_area
    @postcode_area ||= PostcodeArea.new
  end
  helper_method :postcode_area

  protected

  # Perform the API request and validate the resulting data
  def service_area_status
    Sh24::Postcode::ApiReader.new(postcode_area).perform

    ServiceAreaValidator.new(postcode_area).service_area? ? "in" : "out"
  end

  def permitted_params
    params.require(:postcode_area).permit(:postcode)
  end
end
