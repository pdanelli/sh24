# frozen_string_literal: true

class ServiceAreaValidator
  # Iterate over the service areas YML files and memoize the result in a class instance variable to avoid
  # performing the same function more than one per server start
  #
  # @return [Array] of string service area names from the YML
  def self.valid_areas
    @valid_areas ||= begin
      files = Dir.glob(Rails.root.join("config", "service_area_lsoa", "*.yml"))

      return [] if files.blank?

      files.flat_map { |file| YAML.load_file(file) }.uniq
    end
  end

  #
  # @param record [PostcodeArea] the postcode area to be validated
  # @return [Boolean]
  def initialize(record)
    @record = record
  end

  # Is the record in the service areas
  #
  # @return [Boolean]
  def service_area?
    whitelisted? || lsoa_match?
  end

  protected

  # Is the postcode whitelisted
  #
  # @return [Boolean]
  def whitelisted?
    PostcodeValidator.new.valid_whitelist?(@record.postcode)
  end

  # Is the LSOA within the specified service areas
  #
  # @return [Boolean]
  def lsoa_match?
    @record.respond_to?(:lsoa) && @record.lsoa.match?(/#{self.class.valid_areas.join("|")}/)
  end
end
