# frozen_string_literal: true

class PostcodeValidator < ActiveModel::Validator
  # NOTE: Whilst it might be a better approach to send all data to postcodes.io, I wanted to show an example of
  # valiidation without relying souly on the API response. It does however leave out some of the rarer postcodes,
  # like GIR 0AA for example, which will be returned as Invalid.
  #
  # Borrowed from https://stackoverflow.com/a/51885364/3588645
  UK_POSTCODES = /^[A-Z]{1,2}\d[A-Z\d]? ?\d[A-Z]{2}$/i
  WHITELIST_POSTCODES = /^SH\d[A-Z\d]? ?\d[A-Z]{2}$/i

  # Validate the record
  #
  # @param record [PostcodeArea] the postcode area to be validated
  # @return [Nilclass|ActiveModel::Error]
  def validate(record)
    return if valid_whitelist?(record.postcode) || valid_uk?(record.postcode)

    record.errors.add(:postcode, I18n.t("postcode.invalid.format"))
  end

  # Is the string a valid UK postcode
  #
  # @param postcode [String] the postcode to be checked
  # @return [Boolean]
  def valid_uk?(postcode)
    postcode.match?(UK_POSTCODES)
  end

  # Is the string a whitelisted internal postcode
  #
  # @param postcode [String] the postcode to be checked
  # @return [Boolean]
  def valid_whitelist?(postcode)
    postcode.match?(WHITELIST_POSTCODES)
  end
end
