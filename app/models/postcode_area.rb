# frozen_string_literal: true

class PostcodeArea < OpenStruct
  include ActiveModel::Validations

  validates_with PostcodeValidator

  # Overload the `<<` operator to update the attributes using the `<<` syntax
  #
  # @param hash [Hash] a key value hash of values to be updated
  # @return [PostcodeArea]
  #
  # @usage PostcodeArea.new(postcode: "BN3 1AB) << { postcode: "SH24 999" }
  def <<(hash = {})
    hash.each_pair { |field, value| send("#{field}=", value) }

    self
  end

  protected

  # Overload the setter to ensure that our postcode is stripped of leading/trailing whitespace when its updated.
  # This is called from within the :<< method so when we update our instance with user data, it should remove any
  # whitespace.
  #
  # @param postcode [String]
  # @return void
  def postcode=(postcode)
    self[:postcode] = postcode.strip
  end
end
