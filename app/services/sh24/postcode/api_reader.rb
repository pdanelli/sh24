# frozen_string_literal: true

# Makes a request to the Postcode.io API and extract the result
#
# @usage Sh24::PostcodeApiReader.new("bn31ab").perform
module Sh24
  module Postcode
    class ApiReader
      include HTTParty

      base_uri "http://postcodes.io/postcodes/"

      #
      # @param postcode_area [PostcodeArea] the postcode area instance to perform the GET request against
      # @return self
      def initialize(postcode_area)
        @postcode_area = postcode_area
      end

      # Request the data and update the postcode area instance
      #
      # @return [PostcodeArea]
      def perform
        # Ensure we always return a PostcodeArea, even when the request fails
        return @postcode_area unless response.success?

        @postcode_area << response.dig("result")
      end

      protected

      # Perform the GET request to the API endpoint
      #
      # @return [HTTParty::Response] the response from the API request, wrapped in an HTTParty::Response object
      def response
        @response ||= self.class.get(normalized_postcode)
      end

      # Encode the postcode, and ensure the it has a forward slash prefixed to it or HTTParty will parse the URI
      # incorrectly. As these changes are not related to the postcode, but the API request, they are not persisted
      # to the postcode_area instance.
      #
      # @return [String] a forward slash prefixed postcode
      def normalized_postcode
        postcode = URI::Parser.new.escape(@postcode_area.postcode.strip)

        postcode.match?(/^\//) ? postcode : "/#{postcode}"
      end
    end
  end
end

