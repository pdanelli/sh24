# frozen_string_literal: true

RSpec.describe ServiceAreaValidator do
  subject(:validator) { described_class.new(postcode_area) }
  let(:postcode_area) { PostcodeArea.new(postcode_attributes) }
  let(:postcode_attributes) { { postcode: postcode, lsoa: lsoa } }
  let(:postcode) { "SE1 7QD" }
  let(:lsoa) { "Southwark 034A" }
  let(:valid_areas) { %w[Southwark Lambeth] }

  before do
    allow(described_class).to receive(:valid_areas).and_return(valid_areas)
  end

  describe ".new" do
    it "requires one argument" do
      expect { described_class.new }.to raise_error(ArgumentError)
      expect(described_class.new(postcode_area).instance_variable_get(:@record)).to eq postcode_area
    end
  end

  describe "#service_area?" do
    context "when the postcode is not whitelisted but the LSOA matched" do
      it "returns true" do
        expect(validator.service_area?).to be(true)
      end
    end

    context "when the postcode is not whitelisted and the LSOA not matched" do
      let(:lsoa) { "Kensington 012B" }

      it "returns false" do
        expect(validator.service_area?).to be(false)
      end
    end

    context "when the postcode is whitelisted" do
      let(:postcode) { "SH24 1AA" }

      it "returns true" do
        expect(validator.service_area?).to be(true)
      end
    end
  end

  describe "#whitelisted?" do
    context "when the postcode is valid" do
      let(:postcode) { "SH24 1AA" }

      it "returns true" do
        expect(validator.send(:whitelisted?)).to be(true)
      end
    end

    context "when the postcode is not valid" do
      it "returns true" do
        expect(validator.send(:whitelisted?)).to be(false)
      end
    end
  end

  describe "lsoa_match?" do
    context "when the LSOA is a found" do
      it "returns true" do
        expect(validator.send(:lsoa_match?)).to be(true)
      end
    end

    context "when the LSOA is a found" do
      let(:lsoa) { "Kensington 012B" }

      it "returns true" do
        expect(validator.send(:lsoa_match?)).to be(false)
      end
    end
  end
end
