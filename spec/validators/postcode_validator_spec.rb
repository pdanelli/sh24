# frozen_string_literal: true

RSpec.describe PostcodeValidator do
  subject(:validator) { described_class.new }
  let(:postcode_area) { PostcodeArea.new(postcode: postcode) }
  let(:postcode) { "SE1 7QD" }

  it "inherits from ActiveModel::Validator" do
    expect(described_class.ancestors).to include(ActiveModel::Validator)
  end

  describe "#validate" do
    it "responds to validate" do
      expect(validator).to respond_to(:validate)
    end

    context "if the postcode is valid" do
      it "returns nil" do
        expect(validator.validate(postcode_area)).to be_nil
      end
    end

    context "if the postcode is invalid" do
      let(:postcode) { "an invalid postcode" }

      it "returns nil" do
        expect(validator.validate(postcode_area)).to be_a(ActiveModel::Error)
      end
    end

    context "if the postcode is whitelisted" do
      let(:postcode) { "SH24 1AA" }

      it "returns nil" do
        expect(validator.validate(postcode_area)).to be_nil
      end
    end
  end

  describe "#valid_uk?" do
    it "validates a UK postcode" do
      expect(validator.valid_uk?("SE1 7QD")).to be(true)
      expect(validator.valid_uk?("SE17QD")).to be(true)
      expect(validator.valid_uk?("B6 6HE")).to be(true)
      expect(validator.valid_uk?("AAA24 1AA")).to be(false)
      expect(validator.valid_uk?("not a valid postcode")).to be(false)
    end
  end

  describe "#valid_whitelist?" do
    it "validates a SH:24 internal postcode" do
      expect(validator.valid_whitelist?("SH24 1AA")).to be(true)
      expect(validator.valid_whitelist?("SH24 1AB")).to be(true)
      expect(validator.valid_whitelist?("SH241AB")).to be(true)
      expect(validator.valid_whitelist?("SE1 7QD")).to be(false)
      expect(validator.valid_whitelist?("not a valid postcode")).to be(false)
    end
  end
end

