# frozen_string_literal: true

RSpec.describe "checking a postcode service area", type: :feature do
  let(:domain) { "http://postcodes.io/postcodes/" }
  let(:postcode) { "SE1 7QD" }
  let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "postcode_api_reader", "valid_request.json")) }
  # The headers need to be set, or HTTParty will return a JSON string and not parse the data into a Hash
  let(:response_headers) { { "content-type" => ["application/json; charset=utf-8"] } }
  let(:request_url) { "#{domain}#{URI::Parser.new.escape(postcode)}" }
  let(:request_headers) do
    {
      headers: {
        "Accept" => "*/*",
        "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
        "User-Agent" => "Ruby"
      }
    }
  end
  let(:status_code) { 200 }

  before do
    stub_request(:get, request_url).with(request_headers).to_return(
      status: status_code,
      body: response_json,
      headers: response_headers
    )
  end

  context "when visiting the web root" do
    before do
      visit "/"
    end

    it "shows the form" do
      expect(page).to have_content I18n.t("postcode_area.new.heading")
      expect(page).to have_selector("form#postcode_area")
    end
  end

  describe "when filling in a postcode" do
    before do
      visit "/"

      within("#postcode_area") do
        fill_in "Postcode", with: postcode
      end

      click_button "Submit"
    end

    context "when the postcode is inside the service area" do
      it "displays the correct message" do
        expect(page).to have_content I18n.t("postcode_area.service_area.result.in")
      end
    end

    context "when the postcode is whitelisted" do
      let(:postcode) { "SH24 1AA" }

      it "displays the correct message" do
        expect(page).to have_content I18n.t("postcode_area.service_area.result.in")
      end
    end

    context "when the postcode is outside of the service area" do
      let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "postcode_api_reader", "outside_request.json")) }
      let(:postcode) { "B6 6HE" }

      it "displays the correct message" do
        expect(page).to have_content I18n.t("postcode_area.service_area.result.out")
      end
    end

    context "when the postcode is invalid" do
      let(:postcode) { "An invalid postcode" }

      it "displays the correct message" do
        expect(page).to have_content I18n.t("postcode_area.postcode.invalid")
      end
    end
  end
end

