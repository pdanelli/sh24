# frozen_string_literal: true

RSpec.describe PostcodeArea, type: :model do
  subject(:postcode_area) { described_class.new(postcode: postcode) }
  let(:postcode) { "SE1 7QD" }

  it "inherits from OpenStuct" do
    expect(described_class.ancestors).to include(OpenStruct)
  end

  it "inherits from ActiveModel::Validations" do
    expect(described_class.ancestors).to include(ActiveModel::Validations)
  end

  describe "#valid?" do
    it "responds to valid?" do
      expect(postcode_area).to respond_to(:valid?)
    end

    # NOTE: There must be a nicer way to do this, but the validators are not held in the ancestor tree
    it "is validated by the PostcodeValidator" do
      expect(postcode_area._validators.to_a.flatten.compact.first.class).to eq(PostcodeValidator)
    end
  end

  describe "#<<" do
    let(:attributes) { { one: 1, two: 2 } }

    it "responds to <<" do
      expect(postcode_area).to respond_to(:<<)
    end

    it "returns self" do
      pcode = postcode_area << attributes

      expect(postcode_area).to be(pcode)
    end

    it "adds the hash to the instance attributes" do
      postcode_area << attributes

      expect(postcode_area.postcode).to eq(postcode)
      expect(postcode_area.one).to eq(1)
      expect(postcode_area.two).to eq(2)
    end
  end

  describe "#postcode=" do
    subject(:postcode_area) { described_class.new << { postcode: postcode } }

    context "when the postcode has whitespace around it" do
      let(:postcode) { " SE1 7QD " }

      it "returns an uppercased string" do
        expect(postcode_area.postcode).to eq("SE1 7QD")
      end
    end
  end
end
