# frozen_string_literal: true

RSpec.describe Sh24::Postcode::ApiReader do
  let(:api_reader) { described_class.new(postcode_area) }
  let(:postcode_area) { PostcodeArea.new(postcode_attributes) }
  let(:postcode_attributes) { { postcode: postcode } }
  let(:postcode) { "SE17QD" }

  let(:domain) { "http://postcodes.io/postcodes/" }
  let(:response_json) { File.read(Rails.root.join("spec", "fixtures", "postcode_api_reader", "valid_request.json")) }
  # The headers need to be set, or HTTParty will return a JSON string and not parse the data into a Hash
  let(:response_headers) { { "content-type" => ["application/json; charset=utf-8"] } }
  let(:request_url) { "#{domain}#{postcode}" }
  let(:request_headers) do
    {
      headers: {
        "Accept" => "*/*",
        "Accept-Encoding" => "gzip;q=1.0,deflate;q=0.6,identity;q=0.3",
        "User-Agent" => "Ruby"
      }
    }
  end
  let(:status_code) { 200 }

  before do
    stub_request(:get, request_url).with(request_headers).to_return(
      status: status_code,
      body: response_json,
      headers: response_headers
    )
  end

  it "includes HTTParty" do
    expect(described_class.ancestors).to include(HTTParty)
  end

  describe ".new" do
    it "takes an endpoint argument" do
      expect(described_class.new(postcode_area).instance_variable_get(:@postcode_area)).to eq postcode_area
    end
  end

  describe "#perform" do
    subject(:result) { api_reader.perform }

    context "when the request is successful" do
      it "updates the postcode area instance attributes" do
        expect(result).to be_a(PostcodeArea)
      end

      it "returns the expected data" do
        expect(result.postcode).to eq "SE1 7QD"
        expect(result.lsoa).to eq "Southwark 034A"
      end
    end

    context "when the request fails" do
      let(:status_code) { 500 }

      it "returns a hash" do
        expect(result).to be_a(PostcodeArea)
      end

      it "returns the postcode but does not set the LSOA" do
        expect(result.postcode).to eq postcode
        expect(result.respond_to?(:lsoa)).to be(false)
      end
    end
  end

  describe "#response" do
    it "returns the correct response type" do
      expect(api_reader.send(:response)).to be_a(HTTParty::Response)
    end
  end

  describe "#normalized_postcode" do
    context "when there is no space in the postcode" do
      it "creates a URL encoded postcode" do
        expect(api_reader.send(:normalized_postcode)).to eq "/SE17QD"
      end
    end

    context "when there is no space in the postcode" do
      let(:postcode) { "SE1 7QD" }

      it "creates a URL encoded postcode" do
        expect(api_reader.send(:normalized_postcode)).to eq "/SE1%207QD"
      end
    end
  end
end

