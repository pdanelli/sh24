Rails.application.routes.draw do
  root "postcode_areas#new"

  resource :postcode_area, only: %i[new create]
end
