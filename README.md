# Sh:24 Technical Evaluation

## Introduction

Hello Sh:24. This Rails application contains the functionality requested in the technical evaluation for Paul Danelli.

I have avoided complicating the application too many with frontend additions, in order to maintain focus on the quality and extendability of the backend code. I have also removed any of the default Rails features which were unnecessary to the fulfilment of the requirements.

## Installation

The application is self contained within the docker containers and has no external host dependencies. To run the application, please execute the following command in your terminal:

```bash
docker-compose up web
```

The container will then be built and install any Ruby Gem dependencies before starting the Rails server. To view the application please go to `http://localhost:3000` in your browser.

## Testing

To run the tests, execute the following command:

```bash
docker-compose run --rm runner rspec
```

## My code

As requested, I didn't use any scaffolding, but to avoid the need to hunt for my code within the Rails app, please check the following locations:

+ app/controllers
+ app/models
+ app/services
+ app/validators
+ app/views
+ config/locals/en.yml
+ config/service_area_lsoa/SE1.yml
+ spec
